type tsvgIcon = {
  [key: string]: string
}

export const svgIcon: tsvgIcon = {
  microplate96: './assets/icons/Microplate 96.svg'
}
