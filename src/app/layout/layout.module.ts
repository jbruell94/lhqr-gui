import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { LayoutComponent } from "./layout.component";
import { NavbarComponent } from './header/navbar/navbar.component';
import { HeaderActionsComponent } from './header/header-actions/header-actions.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { RouterModule } from "@angular/router";

@NgModule({
  declarations: [
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
    SideBarComponent,
    NavbarComponent,
    HeaderActionsComponent
  ],
  exports: [
    LayoutComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    RouterModule
  ]
})
export class LayoutModule { }
