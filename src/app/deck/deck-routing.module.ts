import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeckPageComponent } from "./pages/deck-page/deck-page.component";

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: DeckPageComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeckRoutingModule { }
