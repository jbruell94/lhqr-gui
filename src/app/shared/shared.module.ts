import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalExampleComponent } from './modals/modal-example/modal-example.component';
import { MatDialogModule } from "@angular/material/dialog";
import { MatButtonModule } from "@angular/material/button";
import { SvgIconComponent } from './components/svg-icon/svg-icon.component';



@NgModule({
  declarations: [
    ModalExampleComponent,
    SvgIconComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule
  ],
  exports: [
    ModalExampleComponent,
    SvgIconComponent
  ]
})
export class SharedModule { }
