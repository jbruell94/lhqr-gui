import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'deck',
    loadChildren: () => import('./deck/deck.module')
      .then(x => x.DeckModule)
  },
  {
    path: 'labware',
    loadChildren: () => import('./labware/labware.module')
      .then(x => x.LabwareModule)
  },
  { path: 'robot', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
