import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeckRoutingModule } from './deck-routing.module';
import { DeckPageComponent } from './pages/deck-page/deck-page.component';
import { SharedModule } from "../shared/shared.module";


@NgModule({
  declarations: [
    DeckPageComponent
  ],
  imports: [
    CommonModule,
    DeckRoutingModule,
    SharedModule
  ]
})
export class DeckModule { }
