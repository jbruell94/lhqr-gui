import { Component } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {ModalExampleComponent} from "../../../shared/modals/modal-example/modal-example.component";

@Component({
  selector: 'app-deck-page',
  templateUrl: './deck-page.component.html',
  styleUrls: ['./deck-page.component.scss']
})
export class DeckPageComponent {

  constructor(public dialog: MatDialog) { }

  testingConfig = {
    names: [
      ['A1', 'A2', 'A3', 'A4'],
      ['B1', 'B2', 'B3', 'B4'],
      ['C1', 'C2', 'C3', 'C4']
    ]
    // names: {
    //   rows: ['A', 'B', 'C'],
    //   cols: [1, 2, 3, 4]
    // }
  }

  openDialog() {
    const dialogRef = this.dialog.open(ModalExampleComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  cellClick(row: string, col: string) {
    console.log(row + col);
  }
}
