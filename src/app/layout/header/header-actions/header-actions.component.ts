import {Component, Input} from '@angular/core';
import { faUser, faBell, faSearch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header-actions',
  templateUrl: './header-actions.component.html',
  styleUrls: ['./header-actions.component.scss']
})
export class HeaderActionsComponent {
  @Input() userName = '';
  faUser = faUser;
  faBell = faBell;
  faSearch = faSearch;
}
