import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';

import { svgIcon } from "./svg-icon.links";

@Component({
  selector: 'app-svg-icon',
  templateUrl: './svg-icon.component.html',
  styleUrls: ['./svg-icon.component.scss']
})
export class SvgIconComponent implements OnChanges {
  @Input() icon = '';
  @Input() name = 'alt';
  iconName = '';

  ngOnChanges(changes: SimpleChanges): void {
    if(changes['icon']) {
      let iconName = changes['icon'].currentValue;
      this.iconName = svgIcon[iconName];
    }
  }
}
