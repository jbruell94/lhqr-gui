import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LabwareRoutingModule } from './labware-routing.module';
import { LabwarePageComponent } from './pages/labware-page/labware-page.component';


@NgModule({
  declarations: [
    LabwarePageComponent
  ],
  imports: [
    CommonModule,
    LabwareRoutingModule
  ]
})
export class LabwareModule { }
