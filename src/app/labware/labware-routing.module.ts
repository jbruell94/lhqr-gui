import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LabwarePageComponent } from "./pages/labware-page/labware-page.component";

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: LabwarePageComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LabwareRoutingModule { }
